<?php

require_once('lib-mail/config.php'); //Файл конфигурации для вашего smtp сервера
require_once('lib-mail/PHPMailerAutoload.php'); //Файл автоматической подгрузки классов PHPMailer

try{
    $mail = new PHPMailer(true); // Создаем экземпляр класса PHPMailer

    $mail->IsSMTP(); // Указываем режим работы с SMTP сервером
    $mail->Host       = $__smtp['host'];  // Host SMTP сервера: ip или доменное имя
    $mail->SMTPDebug  = $__smtp['debug'];  // Уровень журнализации работы SMTP клиента PHPMailer
    $mail->SMTPAuth   = $__smtp['auth'];  // Наличие авторизации на SMTP сервере
    $mail->Port       = $__smtp['port'];  // Порт SMTP сервера
    $mail->SMTPSecure = $__smtp['secure'];  // Тип шифрования. Например ssl или tls
    $mail->CharSet="UTF-8";  // Кодировка обмена сообщениями с SMTP сервером
    $mail->Username   = $__smtp['username'];  // Имя пользователя на SMTP сервере
    $mail->Password   = $__smtp['password'];  // Пароль от учетной записи на SMTP сервере
    $mail->AddAddress('luter.sergei@ya.ru', 'Eskp');  // Адресат почтового сообщения
    $mail->AddAddress('alekseydiv@icloud.com', 'Eskp');  // Адресат почтового сообщения
    $mail->AddReplyTo($__smtp['addreply'], 'First Last');  // Альтернативный адрес для ответа
    $mail->SetFrom($__smtp['username'], $__smtp['mail_title']);  // Адресант почтового сообщения
    $mail->Subject = htmlspecialchars($__smtp['mail_title']);  // Тема письма

    $name = $_POST['fullname'];
    $age = $_POST['age'];
    $sex = $_POST['sex'];
    $text = $_POST['textarea'];

    $templates = serialize($_POST['selected']);

    $message = '';
    $message = " <p>Выбранные шаблоны: " . $templates . "</p> <p>ФИО: ". $name ."</p><p>Возраст: ". $age ." </p><p>Пол: " . $sex  . " <p>Примечания: ". $text . "</p>";

    $mail->MsgHTML($message);

    if ($mail->send()){
        echo 1;
    }
    else{
        echo 0;
    }


} catch (phpmailerException $e) {
    echo 0;
}