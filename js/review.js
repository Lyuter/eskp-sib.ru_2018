$(document).ready(function(){
        $('#review').on('submit', function (event ) {
            event.preventDefault();
            var fullname = $('#fullname').val();
            var sex = $('#sex').val();
            var age = $('#age').val();
            var selected = [];
            $('#review input:checked').each(function() {
                selected.push($(this).attr('value'));
            });
            var textarea = $('#textarea').val();
            // console.log($('#fullname').val());
            // console.log($('#sex').val());
            // console.log($('#age').val());
            // console.log(selected);
            // console.log($('#textarea').val());

            $.ajax({
                type: 'POST',
                url: '../form.php',
                data: {
                    fullname: fullname,
                    sex: sex,
                    age: age,
                    selected: selected,
                    textarea: textarea
                },
                success: function (data) {
                    if (data == 1) {
                        $('#myModal').modal('show');
                    } else {
                        alert("Что-то пошло не так. Пожалуйста, сообщите нам об этой ошибке.");
                    }
                }
            });
        })
}
);