/**
 * Created by Alexey on 12.10.2016.
 */

$(document).ready(function(){
    var offset = 50;
    $('body').scrollspy({ target: '#navbar', offset: offset+10 });


    $('.navbar li a').click(function(event) {
        event.preventDefault();
        $($(this).attr('href'))[0].scrollIntoView();
        scrollBy(0, -offset);
    });
    $('.navbar-collapse a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });
    $('.navbar-collapse button').click(function(){
        $(".navbar-collapse").collapse('hide');
    });

    $('.reviews-slides').bxSlider({
        nextSelector: '.r-slider-control__next',
        prevSelector: '.r-slider-control__prev',
        nextText: ' ',
        prevText: ' ',
        onSliderLoad: function () {
            $(".reviews-slider .bx-pager.bx-default-pager").remove();
        }
    });

    $('.callback__btn, .header__btn, .reason__btn, .price-item__btn').click(function (e) {
        if($(this).hasClass('reason__btn')) {
            $('.popup-callback .popup__title').css('font-size', '21px').text($(this).parents('.reason').find('.reason__desc').text());
            $('.popup-callback input[name=form]')
                .val($(this).parents('.reason').find('.reason__desc').text().replace(/\s{2,}/g, ' '));
        } else {
            $('.popup-callback .popup__title').css('font-size', '23px').text($(this).text());
            $('.popup-callback input[name=form]')
                .val($(this).text().replace(/\s{2,}/g, ' '));
        }
        e.preventDefault();

        $('.overlay').fadeIn(400,
            function () {
                $('.popup-callback')
                    .css('display', 'block')
                    .animate({opacity: 1}, 200);
            });
        return false;
    });

    $('.popup__close, .overlay').click(function () {
        $('.popup')
            .animate({opacity: 0}, 200,
            function () {
                $(this).css('display', 'none');
                $('.overlay').fadeOut(400);
            }
        );
    });

    $('.header-decor__item').each(function(){
        var $bgobj = $(this);
        $(window).scroll(function() {
            var yPos = ($(window).scrollTop() / $bgobj.data('speed')) + $bgobj.data('pos');
            $bgobj.css({ top: yPos });
        });
    });

    // Contact form

    $('form').submit(function(e){
        e.preventDefault();
        var self = this;
        var clientName = $(this).find('.clientName');
        var clientPhone = $(this).find('.clientPhone');
        console.log(this, clientName.val(),clientPhone.val());
        $.ajax({
            url: $(this).attr('action'),
            data: "message=" + clientName.val() + "&phone=" + clientPhone.val(),
        }).done(function(data){
            console.log(data);
            if (data == "1")
            {
                $(self).parents('.popup')
                    .animate({opacity: 0}, 200,
                        function () {
                            $(this).css('display', 'none');
                        }
                    );
                $('.overlay').fadeIn(400,
                    function () {
                        $('.popup-thank')
                            .css('display', 'block')
                            .animate({opacity: 1}, 200);
                    });
            }
            else
            {
                alert('Произошла ошибка!');
            }
        });
    });

    $('.price__more').on('click', function(){
        if($(this).hasClass('open')) {
            $('.price-list_add').slideUp();
            $(this).removeClass('open').find('span').text('Развернуть прайс');
        } else {
            $('.price-list_add').slideDown();
            $(this).addClass('open').find('span').text('Свернуть прайс');
        }
    });

    $("input[name=phone]").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 189]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
            (e.keyCode == 57 && e.shiftKey === true) ||
                // Allow: Ctrl+C
            (e.keyCode == 48 && e.shiftKey === true) ||
                // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

